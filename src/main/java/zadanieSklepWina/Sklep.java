package zadanieSklepWina;

public class Sklep {
    public static void zrobZakup(double kwota, KartaKlienta kartaKlienta) {

        if(kwota>50 && kwota <=100){
            kwota =    kwota - (kwota * 0.05);
        } else if (kwota > 100) {
            kwota = kwota - (kwota * 0.1);
        } else if (kwota > 50 && kwota <= 100 && kartaKlienta.pobierzLiczbeKuponow() >= 10) {
            kwota =    kwota - (kwota * 0.08);
        }else if (kwota > 100 && kartaKlienta.pobierzLiczbeKuponow()>=10) {
            kwota = kwota - (kwota * 0.13);
        }
        if (40 <= kwota && kwota < 80) {
            kartaKlienta.setLiczbaKuponow(kartaKlienta.pobierzLiczbeKuponow()+ 1);
        } else if (kwota >= 80 && kwota < 120) {
            kartaKlienta.setLiczbaKuponow(kartaKlienta.pobierzLiczbeKuponow() + 2);
        } else if (kwota >= 120) {
            kartaKlienta.setLiczbaKuponow(kartaKlienta. pobierzLiczbeKuponow() +3);
        }
    }
}
