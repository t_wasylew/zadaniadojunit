package zadanieSklepWina;

public class KartaKlienta {

    private int liczbaKuponow;

    public KartaKlienta() {
        this.liczbaKuponow = 0;
    }

    public void setLiczbaKuponow(int liczbaKuponow) {
        this.liczbaKuponow = liczbaKuponow;
    }

    public int pobierzLiczbeKuponow() {
        return liczbaKuponow;
    }
}
