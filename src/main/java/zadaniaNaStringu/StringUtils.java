package zadaniaNaStringu;

public class StringUtils {

    public static String changeLowerCaseToUpperCase(String string) {
        return string.toUpperCase();
    }

    public static String reverseString(String string) {
        return new StringBuilder(string).reverse().toString();
    }

    public static String multiplyString(String string, int number) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < number; i++) {
            stringBuffer.append(string);
        }
        return stringBuffer.toString();
    }

    public static String removeUpperCase(String string) {
        return string.replaceAll("[A-Z]", "");
    }
    public static String removeLowerCase(String string) {
        return string.replaceAll("[a-z]", "");
    }

    public static String removeAlphanumericChars(String s) {
        return s.replaceAll("(\\W|\\d)", "");
    }
}
