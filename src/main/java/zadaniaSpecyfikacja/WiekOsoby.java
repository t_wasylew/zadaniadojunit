package zadaniaSpecyfikacja;

import javax.swing.text.DateFormatter;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class WiekOsoby {

    /**
     * Metoda obliczająca wiek osoby w danym dniu
     * @param dataUrodzenia - wymagany format "dd.MM.yyyy"
     * @param wybranaData - wymagany format "dd.MM.yyyy"
     *                    wymagane: wybranaData >= dataUrodzenia
     * @return wiek osoby
     * @throws IllegalArgumentException kiedy warunki nie są spełnione
     */
    public static int obliczWiek(String dataUrodzenia, String wybranaData) {

        if(!(dataUrodzenia.matches("\\d{2}.\\d{2}.\\d{4}") &&
                wybranaData.matches("\\d{2}.\\d{2}.\\d{4}"))){
            throw new IllegalArgumentException("Niepoprawny format daty");
        }
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        LocalDate parsedDataUrodzenia = LocalDate.parse(dataUrodzenia, dateTimeFormatter);
        LocalDate parsedWybranaData = LocalDate.parse(wybranaData, dateTimeFormatter);


        int years;
        if (parsedDataUrodzenia.isBefore(parsedWybranaData)) {
          years = (int) ChronoUnit.YEARS.between(parsedDataUrodzenia, parsedWybranaData);
        } else if (parsedDataUrodzenia.isEqual(parsedWybranaData)) {
            years = 0;
        }else {
            throw new IllegalArgumentException("Wybrana data >= data urodzenia");
        }
        return years;

    }
}
