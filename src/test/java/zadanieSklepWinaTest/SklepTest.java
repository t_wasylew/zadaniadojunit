package zadanieSklepWinaTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import zadanieSklepWina.KartaKlienta;
import zadanieSklepWina.Sklep;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SklepTest {

    @Parameterized.Parameter(value = 1)
    public int liczbaKuponow;

    @Parameterized.Parameter (value = 0)
    public double kwota;

    KartaKlienta kartaKlienta;

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {39.99d, 0},
                {40.00d, 1},
                {79.99d, 1},
                {80.00d, 1},
                {119.99d, 2},
                {120.00d, 2},
                {84.22d, 2},
                {84.20d, 1},
        });
    }

    @Before
    public void utworzenieKarty() {
        kartaKlienta = new KartaKlienta();
    }

    @Test
    public void zrobZakupyTest() {
        Sklep.zrobZakup(kwota, kartaKlienta);
        assertEquals(liczbaKuponow, kartaKlienta.pobierzLiczbeKuponow());
    }
}
