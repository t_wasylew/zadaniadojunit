package zadanieSklepWinaTest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import zadanieSklepWina.KartaKlienta;
import zadanieSklepWina.Sklep;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SklepTest2 {
    @Parameterized.Parameter(value = 1)
    public int liczbaKuponow;

    @Parameterized.Parameter (value = 0)
    public double kwota;

    KartaKlienta kartaKlienta;

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {41.23d, 10},
                {41.24d, 11},
                {86.95d, 11},
                {86.96d, 11},
                {137.93d, 12},
                {137.94d, 12},
        });
    }
    @Before
    public void utworzenieStalegoKlienta() {
        kartaKlienta = new KartaKlienta();
        Sklep.zrobZakup(200,kartaKlienta);
        Sklep.zrobZakup(200,kartaKlienta);
        Sklep.zrobZakup(200,kartaKlienta);
        Sklep.zrobZakup(60,kartaKlienta);
    }

    @Test
    public void zrobZakupyTest() {
        Sklep.zrobZakup(kwota, kartaKlienta);
        assertEquals(liczbaKuponow, kartaKlienta.pobierzLiczbeKuponow());
    }
}
