package zadaniaNaStringuTest;

import org.junit.Test;
import zadaniaNaStringu.StringUtils;


import static org.junit.Assert.assertEquals;
public class RemoveAlphanumericChars {

    @Test
    public void remove123From123Test(){
        assertEquals("Test", StringUtils.removeAlphanumericChars("123Test*%"));
    }
}
