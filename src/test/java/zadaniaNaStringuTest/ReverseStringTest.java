package zadaniaNaStringuTest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static zadaniaNaStringu.StringUtils.reverseString;

public class ReverseStringTest {

    @Test
    public void reverseTomekToKemot() {
        assertEquals("kemoT", reverseString("Tomek"));
    }

    @Test
    public void reverseKajakTokajaK() {
        assertEquals("kajaK", reverseString("Kajak"));
    }

    @Test
    public void reverse12345To54321() {
        assertEquals("54321", reverseString("12345"));
    }
}
