package zadaniaNaStringuTest;

import org.junit.Test;
import  static zadaniaNaStringu.StringUtils.changeLowerCaseToUpperCase;

import static org.junit.Assert.assertEquals;

public class ChangeLowerCaseToUpperCaseTest {

    @Test
    public void allLowerCaseStringToUpperCase() {
        assertEquals("TEST", changeLowerCaseToUpperCase("test"));
    }

    @Test
    public void allUpperCaseStringToUpperCase() {
        assertEquals("TEST", changeLowerCaseToUpperCase("TEST"));
    }
}
