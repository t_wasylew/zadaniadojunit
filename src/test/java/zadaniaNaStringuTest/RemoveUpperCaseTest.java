package zadaniaNaStringuTest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static zadaniaNaStringu.StringUtils.removeUpperCase;
public class RemoveUpperCaseTest {

    @Test
    public void removeTW_FromTomaszWasylew() {
        assertEquals("omasz asylew", removeUpperCase("Tomasz Wasylew"));
    }
    @Test
    public void removeTW_FromTomaszWasylew555() {
        assertEquals("omasz asylew555", removeUpperCase("Tomasz Wasylew555"));
    }

}
