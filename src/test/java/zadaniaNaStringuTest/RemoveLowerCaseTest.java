package zadaniaNaStringuTest;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static zadaniaNaStringu.StringUtils.removeLowerCase;
import static zadaniaNaStringu.StringUtils.removeUpperCase;

public class RemoveLowerCaseTest {


    @Test
    public void remove_omasz_asylew_FromTomaszWasylew() {
        assertEquals("T W", removeLowerCase("Tomasz Wasylew"));
    }

    @Test
    public void remove_omasz_asylew_FromTomaszWasylew555() {
        assertEquals("T W555", removeLowerCase("Tomasz Wasylew555"));
    }
}

