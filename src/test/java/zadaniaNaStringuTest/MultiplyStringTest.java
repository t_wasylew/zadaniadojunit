package zadaniaNaStringuTest;

import org.junit.Test;

import static zadaniaNaStringu.StringUtils.multiplyString;
import static org.junit.Assert.assertEquals;
public class MultiplyStringTest {

    @Test
    public void multiplyTestThreeTimes() {
        assertEquals("testtesttest", multiplyString("test", 3));
    }
}
