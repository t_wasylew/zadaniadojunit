package zadaniaSpecyfikacjaTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import zadaniaSpecyfikacja.WiekOsoby;

import static org.junit.Assert.assertEquals;

public class WiekOsobyTest {

    private static final String NIEPOPRAWNY_FORMAT_DATY = "2000.01.01";
    private static final String POPRAWNY_FORMAT_DATY = "2000.01.01";
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void niewpoprawnyFormatDatyUrodzenia() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Niepoprawny format daty");
        WiekOsoby.obliczWiek(NIEPOPRAWNY_FORMAT_DATY, POPRAWNY_FORMAT_DATY);
    }
    @Test
    public void niewpoprawnyFormatWybranejUrodzenia() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Niepoprawny format daty");
        WiekOsoby.obliczWiek(POPRAWNY_FORMAT_DATY, NIEPOPRAWNY_FORMAT_DATY);
    }

    @Test
    public void nieprawidłowyDoborDat() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Wybrana data >= data urodzenia");
        WiekOsoby.obliczWiek("02.01.2000", "01.01.2000");
    }

    @Test
    public void oczekiwanyWiek0() {
        assertEquals(0,WiekOsoby.obliczWiek("01.01.2000","01.01.2000" ));
    }

    @Test
    public void oczekiwanyWiek10() {
        assertEquals(10,WiekOsoby.obliczWiek("02.01.2000","02.01.2010" ));
    }

    @Test
    public void oczekiwanyWiek9() {
        assertEquals(9,WiekOsoby.obliczWiek("02.01.2000","01.01.2010" ));
    }
}
